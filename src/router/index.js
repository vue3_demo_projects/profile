import { createRouter, createWebHistory } from 'vue-router'
import { useUserProfileStore } from "@/store"

import SignUpPage from '@/pages/SignUpPage.vue'
const routes = [
  { 
      path: '/signup',
      name: 'signup',
      component: SignUpPage,
      meta: {
        auth: false
      }
  },
  { 
      path: '/',
      name: '/',
      component: SignUpPage,
      meta: {
        auth: false
      }
  },
  { 
      path: '/home',
      name: 'home',
      component: () => import('@/pages/MainPage.vue'),
      meta: {
        auth: true
      }
  },
  {
      path: '/users-page',
      name: 'usersPage',
      component: () => import('@/pages/UsersPage.vue'),
      meta: {
        auth: true
      }
  },
  {
      path: '/user-profile-page',
      name: 'user-profile-page',
      component: () => import('@/pages/user/UserProfilePage.vue'),
      meta: {
        auth: true
      }
  },
  {
    path: '/user-profile-edit-page/:id',
    name: 'user-profile-edit-page',
    component: () => import('@/pages/user/UserProfileEditPage.vue'),
    meta: {
      auth: true
    }
  },
  {
      path: '/users-control-page',
      name: 'users-control-page',
      component: () => import('@/pages/UsersControlPage.vue'),
      meta: {
        auth: true
      }
  },
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const store = useUserProfileStore()
  if (to.meta.auth && !store.userInfo.accessToken) {
    next('/signup')
  } else if (!to.meta.auth && store.userInfo?.accessToken) {
    next('/home')
  } else {
    next()
  }
})


export default router