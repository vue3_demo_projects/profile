import axios from 'axios'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useRouter } from 'vue-router'
import axiosApiInstance from '../api'
const host = "http://localhost:3030"

export const useUserProfileStore = defineStore('userProfile', () => {
    const router = useRouter()
    const userList = ref([])
    const userInfo = ref({
        accessToken: '',
        refreshToken: ''
    })
    // сохранение токенов
    const logout = () => {
        userInfo.value.accessToken = null
        userInfo.value.refreshToken = null
        localStorage.removeItem('userTokens')
        router.push({path: 'signup'})
    }
    const saveTokens = (data) => {
        userInfo.value = {
            accessToken: data.tokens.accessToken,
            refreshToken: data.tokens.refreshToken
        }
        localStorage.setItem(
            'userTokens',
            JSON.stringify({
                accessToken: data.tokens.accessToken,
                refreshToken: data.tokens.refreshToken
            })
        )
    }
    /**
     * TODO обновление токена и списка пользователей
     * Получение новых токенов
     * вынести на middleware
     * */ 
    const refreshExpiredToken = async () => {
        const refreshExpiredTokenResult = await axios.put(`${host}/api/auth/refreshTokens`,
            { 
                "refreshToken" : `${userInfo.value.refreshToken}`
            })
            const statusCode = refreshExpiredTokenResult.data.statusCode
        if(statusCode === 422 || statusCode === 401) {
            logout()
        } else { // обновляем токены
            saveTokens(refreshExpiredTokenResult.data)
            router.push('/home')
        }
    }
    /**
     * Получаем список пользователей
     */
    const getAllUsers = async () => {
        
        try {
            const getAllUsersResult = await axiosApiInstance.get(`${host}/api/getallusers`)
            if (getAllUsersResult.data.statusCode === 401) {
                refreshExpiredToken()
            } else {
                userList.value = getAllUsersResult.data
            }
        } catch(err) {
            console.log('err message: ', err)
        }
    }
    const getOne = async (id) => {
        try {
              return await axiosApiInstance.get(`${host}/api/user/${id}`)
        } catch(err) {
            console.log('err message: ', err.message)
        }
    }
    const updateUser = async (data) => {
        try {
            const result = await axiosApiInstance.patch(`${host}/api/user/${data.id}`, data)
            return result.data
        } catch(err) {
            console.log('err message: ', err.message)
        }
    }
    const deleteUser = async (id) => {
        try {
            const result = await axiosApiInstance.delete(`${host}/api/user/${id}`)
            return result.data
        } catch(err) {
            console.log('err message: ', err.message)
        }
    }
    const signupUser = async (data) => {
        try {
            const result = await axiosApiInstance.post(`${host}/api/auth/signup`, {
                login: data.login.value,
                fields: [
                    { password : data.password.value },
                    { login: data.login.value }
                ]
            })
            return result.data
        } catch(err) {
            console.log('signup err message: ', err.message)
        }
    }
    const signinUser = async (data) => {
        try {
            const signinResult = await axios.post(`${host}/api/auth/signin`, {
                login: data.login.value,
                fields: [
                    { password : data.password.value },
                    { login: data.login.value }
                ]
            })
            saveTokens(signinResult.data)
            return signinResult.data
        } catch(err) {
            console.log('signup err message: ', err.message)
        }
    }
    const isUserListEmpty = () => {
        if(userList.value.length === 0) {
            getAllUsers()
        }
    }

  return {
    userList,
    getAllUsers,
    getOne,
    updateUser,
    signupUser,
    signinUser,
    userInfo,
    logout,
    deleteUser,
    isUserListEmpty
  }

})