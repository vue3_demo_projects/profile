import axios from 'axios'
import { useUserProfileStore } from './store'

const axiosApiInstance = axios.create()

axiosApiInstance.interceptors.request.use(config => {
    const store = useUserProfileStore()
    const tokens = JSON.parse(localStorage.getItem('userTokens'))
    const setToken = tokens?.accessToken ? tokens.accessToken : store.userInfo.accessToken
    const url = config.url
    if (!url.includes('signin') && !url.includes('signup')) {
        config.headers = {
            "Content-Type": 'application/json',
            "authorization" : `Bearer${setToken}`} 
        }
    return config
    
})

export default axiosApiInstance